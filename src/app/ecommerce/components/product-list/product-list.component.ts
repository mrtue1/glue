import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  public newUrl: string = '';

  constructor(private router: Router) {

  }

  ngOnInit() {
    const currentUrl = this.router.url;
    this.newUrl = currentUrl + '/id';
  }

}
