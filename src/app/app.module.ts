import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContentComponent } from './components/content/content.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { ContactComponent } from './ecommerce/components/contact/contact.component';
import { DashboardComponent } from './ecommerce/components/dashboard/dashboard.component';
import { IntroComponent } from './ecommerce/components/intro/intro.component';
import { NewsDetailComponent } from './ecommerce/components/news-detail/news-detail.component';
import { NewsComponent } from './ecommerce/components/news/news.component';
import { ProductDetailComponent } from './ecommerce/components/product-detail/product-detail.component';
import { ProductListComponent } from './ecommerce/components/product-list/product-list.component';

@NgModule({
  declarations: [
    AppComponent,
    ContentComponent,
    FooterComponent,
    HeaderComponent,

    DashboardComponent,
    IntroComponent,
    NewsComponent,
    NewsDetailComponent,
    ProductListComponent,
    ProductDetailComponent,
    ContactComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
