import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactComponent } from './ecommerce/components/contact/contact.component';
import { DashboardComponent } from './ecommerce/components/dashboard/dashboard.component';
import { IntroComponent } from './ecommerce/components/intro/intro.component';
import { NewsDetailComponent } from './ecommerce/components/news-detail/news-detail.component';
import { NewsComponent } from './ecommerce/components/news/news.component';
import { ProductDetailComponent } from './ecommerce/components/product-detail/product-detail.component';
import { ProductListComponent } from './ecommerce/components/product-list/product-list.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
  },
  {
    path: 'introduce',
    component: IntroComponent,
  },
  {
    path: 'tile-adhesive',
    component: ProductListComponent
  },
  {
    path: 'tile-adhesive/:productCode',
    component: ProductDetailComponent
  },
  {
    path: 'plaster-adhesive',
    component: ProductListComponent
  },
  {
    path: 'plaster-adhesive/:productCode',
    component: ProductDetailComponent
  },
  {
    path: 'construction-chemicals',
    component: ProductListComponent
  },
  {
    path: 'construction-chemicals/:productCode',
    component: ProductDetailComponent
  },
  {
    path: 'consulting',
    component: NewsComponent
  },
  {
    path: 'consulting/:newsCode',
    component: NewsDetailComponent
  },
  {
    path: 'news',
    component: NewsComponent
  },
  {
    path: 'news/:newsCode',
    component: NewsDetailComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: '**', redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
